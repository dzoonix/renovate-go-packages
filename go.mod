module golang/test

go 1.16

require (
	github.com/Azure/azure-sdk-for-go v37.2.0+incompatible
        github.com/gofrs/uuid v3.4.0+incompatible
	golang.org/x/crypto v0.0.0-20210415154028-4f45737414dc
	golang.org/x/oauth2 v0.0.0-20210413134643-5e61552d6c78
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
